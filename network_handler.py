from fetchbot.affiliate_networks import *

class NetworkHandler:
    def __init__(self, args):
        self.networks = []
        
        if args['network'] in ('pj', 'pepperjam'):
            self.networks.append(PepperJamNetwork( {'period': args['period']} ))
        elif args['network'] in ('sas', 'shareasale'):
            self.networks.append(SASNetwork( {'period': args['period']} ))
        elif args['network'] == 'all':
    	    self.networks.extend((SASNetwork( {'period': args['period']} ), PepperJamNetwork( {'period': args['period']} )))
    	    
    def build_response(self):
        #something with self.networks and response building
        message = ""
        for i, n in enumerate(self.networks):
            if i == 0:
                message = n.message()
            else:
                message = message + "\n" + n.message()
            
        return message