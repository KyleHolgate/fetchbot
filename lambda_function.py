from fetchbot.user_request import UserRequest
import secrets as secrets
import urllib
import requests as r
import json

def lambda_handler(event, context):
    body = event['body']
    
    ur = UserRequest(body)
    sales = ur.get_sales()
    print(sales)
    
    
    response = r.post(
        urllib.parse.unquote(ur.response_hook), json={'attachments':[{'text': sales, 'color': 'good'}], 'response_type': 'in_channel'},
        headers={'Content-Type': 'application/json'}
    )
    
    return { "isBase64Encoded": True, "statusCode": 200, "headers": { }, "body": "" }