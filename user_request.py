import re
from fetchbot.network_handler import NetworkHandler

class UserRequest():
    def __init__(self, body):
        self.body = body
        self.response_hook = re.search('\&response_url=(.*?)\&', self.body).group(1)
        self.params = re.search('\&text=(.*?)\&', self.body).group(1)
        self.user = re.search('\&user_id=(.*?)\&', self.body).group(1)
        
    @property
    def network(self):
        try:
            self._network = re.search('^[a-zA-Z]+', self.params).group()
        except:
            self._network = 'all'
            
        return self._network
        
    @property
    def period(self):
        try:
            self._period = re.search('^(\w+)[ |\+](.*)', self.params).group(2)
        except:
            self._period = 'today'
            
        return self._period
        
    def get_sales(self):
        nh = NetworkHandler({'network': self.network, 'period': self.period})
        
        return self.append_user_emoji(nh.build_response())
        
    def append_user_emoji(self, message):
        if self.user == 'U92KV49PU':
            message = message + ' :derrick:'
        else:
            message = message + ' :crown:'
            
        return message