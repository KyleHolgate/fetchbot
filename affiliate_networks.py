from abc import ABC, abstractmethod
from time import strftime, gmtime
import datetime
import re
import secrets as secrets
import hashlib
import json
import requests as r

class AffiliateNetwork(ABC):
    
    @abstractmethod
    def fetch_revenue(self):
        pass
    
    @property
    @abstractmethod
    def name(self):
        pass
    
    @property
    @abstractmethod
    def period(self):
        pass

    @abstractmethod
    def message(self):
        pass
    
    def get_start_stop_dict(self):
        if self.period == 'today':
            start_date = (datetime.datetime.today()-datetime.timedelta(hours=5))
            end_date = (datetime.datetime.today()-datetime.timedelta(hours=5))
        elif self.period == 'yesterday':
            start_date = (datetime.datetime.today()-datetime.timedelta(hours=29))
            end_date = (datetime.datetime.today()-datetime.timedelta(hours=29))
        elif re.match('(\d+)?weeks?', self.period):
            reg_match = re.match('(\d+)?weeks?', self.period)
            num = int(reg_match.group(1))
                			
            start_date = ((datetime.datetime.today()-datetime.timedelta(hours=5))-datetime.timedelta(days=7*num))
            end_date = (datetime.datetime.today()-datetime.timedelta(hours=5))
        elif self.period == 'mtd':
            start_date = (datetime.datetime.today().replace(day=1))
            end_date = (datetime.datetime.today()-datetime.timedelta(hours=5))
            			
        return {'start': start_date, 'end': end_date}
        
    def build_message_str(self, sales_data = {'sales' : "0", 'earnings' : "0"}):
    	zero_list = [0, "0", "0.00"]
    	#print("in bulid msg str::")
    	#print(sales_data)
    	
    	if sales_data['sales'] not in zero_list:
    		message = "*"+self.name+"*" + " :moneybag: Sales: " + str(sales_data['sales']) + ", Earnings: $" + str(sales_data['earnings'])
    	else:
    		message = "*"+self.name+"*" + " :cry: No sales"
    		
    	if self.period in ["today", "yesterday"]:
    		message = message + " " + self.period
    	elif self.period == 'mtd':
    		message = message + " " + datetime.datetime.now().strftime('%B') + " to date"
    	elif re.match('(\d+)?weeks?', self.period):
    		reg_match = re.match('(\d+)?weeks?', self.period)
    		num = int(reg_match.group(1))
    			
    		if num == 1:
    			message = message + " past week"
    		else:
    			message = message + " last "+str(num)+" weeks"
    			
    	return message
    
class SASNetwork(AffiliateNetwork):
    def __init__(self, args):
        self._period = args['period']
        
    @property
    def name(self):
        return 'Share a Sale'
        
    @property
    def period(self):
        return self._period
        
    def message(self):
        sales_data = self.fetch_revenue()
        msg = self.build_message_str(sales_data)
        return msg
    
    def fetch_revenue(self):
    	my_affiliate_id = '1519414'
    	api_token       = secrets.sas_token
    	api_secret_key  = secrets.sas_key
    	my_timestamp    = strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime())
    	api_version     = '2.3';
    	action_verb     = 'activity';
    	timeframe = self.get_start_stop_dict()
    #print(timeframe)
    		
    	# setup request params
    	params = {'affiliateId' : my_affiliate_id, 'token' : api_token, 'version' : api_version, 'action' : action_verb, 'dateStart' : timeframe['start'].strftime('%m/%d/%Y'), 'dateEnd' : timeframe['end'].strftime('%m/%d/%Y'), 'format' : 'xml'}
    	sig        = api_token + ':' + my_timestamp + ':' + action_verb + ':' + api_secret_key;
    	sig_hash   = hashlib.sha256(sig.encode('utf-8')).hexdigest()
    	headers = {'x-ShareASale-Date' : my_timestamp, 'x-ShareASale-Authentication' : sig_hash}
    	call_url = 'https://api.shareasale.com/x.cfm?'
    	req = r.get(call_url, params=params, headers=headers)
    	
    	#print(req.text)
    
    	count = 0
    	commission = 0
    	tt_p = re.compile('(sale|.*bounty)', re.I)
    	for rec in re.finditer('<activitydetailsreportrecord>(.*?)</activitydetailsreportrecord>', req.text, re.DOTALL):
    		if (
    			tt_p.match(re.search('<transtype>(.*?)</transtype>', rec.group(1)).group(1)) and
    			re.search('<voided>(.*?)</voided>', rec.group(1)).group(1) != '1'
    			):
    			count = count + 1
    			commission = commission + float(re.search('<commission>([0-9\.\,]+)</commission>', rec.group(1)).group(1))
    
    	return {'sales' : count, 'earnings' : commission}
    	
    	
class PepperJamNetwork(AffiliateNetwork):
    def __init__(self, args):
        self._period = args['period']
        
    @property
    def name(self):
        return 'PepperJam'
        
    @property
    def period(self):
        return self._period
        
    def message(self):
        sales_data = self.fetch_revenue()
        msg = self.build_message_str(sales_data)
        return msg
    
    def fetch_revenue(self):
        base_url = "https://api.pepperjamnetwork.com/20120402/" #{resource}?apiKey={apiKey}&format={format}
        api_key = secrets.pepperjam_key #os.environ[key]
    		
        timeframe = self.get_start_stop_dict()
    		
        call_url = base_url+"publisher/report/transaction-summary?apiKey="+api_key+"&format=json"+"&startDate="+timeframe['start'].strftime('%Y-%m-%d')+"&endDate="+timeframe['end'].strftime('%Y-%m-%d')
        #print(call_url)
        req = r.get(call_url)
        rev_json = req.json()
    	
        try:
            data = rev_json['data'][0]
        except IndexError:
            data = {'sale_count' : 0, 'total_commission' : 0}
    
        return {'sales' : data['sale_count'], 'earnings' : data['total_commission']}    